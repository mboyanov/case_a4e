import rnn
import numpy as np
datasets = rnn.getDatasets(5, {"hello": 0, "world":1}, ["hello hello world", "who are you world hello"])
print(datasets[0])
assert(datasets[0] is not None)
assert(np.array_equal(datasets[0][0], np.array([0, 0])))
assert(np.array_equal(datasets[0][1], np.array([1])))
print(datasets[1][0])
assert(np.array_equal(datasets[1][0], np.array([0,1])))