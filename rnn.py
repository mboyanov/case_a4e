# LSTM for sequence classification in the IMDB dataset
import numpy
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM, Dropout
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.metrics import sparse_categorical_crossentropy
from gensim.models import Word2Vec
import numpy as np
from keras.utils.np_utils import to_categorical


ds = 'repeated-2'
w2v_model = Word2Vec.load('models/basketvectors-%s.csv' % ds)
vocab = w2v_model.wv.vocab
simple_vocab = {}
for w in vocab:
    simple_vocab[w] = vocab[w].index + 1
sorted_vocab = sorted(simple_vocab.items(), key = lambda x: x[1])
rev_vocab = [y[0] for y in sorted_vocab]
weights = [np.zeros(50)]
weights.extend([w2v_model.wv[y] for y in rev_vocab])

weights = np.array(weights)

print("w2v model loaded")
# fix random seed for reproducibility
numpy.random.seed(7)
# load the dataset but only keep the top n words, zero the rest

def getDatasets(num_train, vocab, file):
    X_train = []
    y_train = []
    X_test = []
    y_test = []
    for i, line in enumerate(file):
        transaction_items = np.array([vocab[x] for x in line.split() if x in vocab])
        if i < num_train:
            X_train.append(transaction_items[:-1])
            y_train.append(transaction_items[-1])
        else:
            X_test.append(transaction_items[:-1])
            y_test.append(transaction_items[-1])
    return X_train, to_categorical(y_train, len(vocab) +1), X_test, to_categorical(y_test, len(vocab) + 1)


if __name__ != "main":
    basket_in, basket_out, _, _= getDatasets(750000, simple_vocab, open('data/basket_rules.csv'))
    X_train, y_train, X_test, y_test = getDatasets(800000, simple_vocab, open('data/transactions-%s.csv' % ds))
    print("dataset loaded")
    max_basket_length = 10
    X_train = sequence.pad_sequences(X_train, maxlen=max_basket_length)
    X_test = sequence.pad_sequences(X_test, maxlen=max_basket_length)
    basket_in = sequence.pad_sequences(basket_in, maxlen=max_basket_length)
    # create the model
    embedding_vector_length = 50
    model = Sequential()
    model.add(Embedding(len(simple_vocab) + 1, embedding_vector_length, mask_zero=True, input_length=max_basket_length, weights=[weights]))
    model.add(Dropout(0.3))
    model.add(LSTM(30, dropout_W=0.3))
    model.add(Dense(len(simple_vocab)+1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['sparse_categorical_accuracy', "accuracy"])
    print(model.summary())
    model.fit(X_train, y_train, nb_epoch=2, batch_size=512, verbose=1, validation_data=(X_test, y_test))
    model_json = model.to_json()
    with open("model-%s.json" % ds, "w") as json_file:
        json_file.write(model_json)

    # Final evaluation of the model
    scores = model.evaluate(X_test, y_test, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1]*100))
    scores = model.evaluate(basket_in, basket_out, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1]*100))
    predictions = model.predict(basket_in)
    print(predictions)
    top_3 = predictions.argsort()[-3:][::-1]
    for prediction in predictions:
        top_3 = prediction.argsort()[-3:][::-1]
        for item in top_3:
            print(rev_vocab[item-1], prediction[item])
    predictions = np.argmax(predictions, axis=1)
    print(predictions)
    for prediction in predictions:
        print(rev_vocab[prediction-1])
