from collections import defaultdict
from numpy.random import permutation as random_permutation
from itertools import permutations

transactions = defaultdict(lambda: [])
num_transactions = 0
unique_categories = set()
unique_products = set()

i = 0
for line in open('data/DataSet1.csv'):
    if i== 0:
        i+=1
        continue
    data = line.split(",")
    if len(data[0]) > 0:
        transactions[data[0]].append([x.strip() for x in data[1:]])
        num_transactions += 1
        unique_products.add(data[3])
        unique_categories.add(data[2])
    else:
        print(line)
import math


def persistTransactionsRaw(transactions,fn):
    with open(fn, 'w') as out:
        for _, transaction in transactions.items():
            items_in_transaction = len(transaction)
            if items_in_transaction < 3:
                continue
            products = [" ".join(item[2] for _ in range(round(math.log(float(item[3])+1, 2)))) for item in transaction]
            if items_in_transaction < 5:
                for i, permutation in enumerate(permutations(products)):
                    out.write(" ".join(permutation) + "\n")
            else:
                for i in range(24):
                    permutation = random_permutation(products)
                    out.write(" ".join(permutation) + "\n")


def getTotalItemsInTransaction(transaction):
    return sum([round(float(x[3])) for x in transaction])


persistTransactionsRaw(transactions, 'data/transactions-repeated-2.csv')