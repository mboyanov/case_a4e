from gensim.models import Word2Vec
from gensim.models.word2vec import LineSentence
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from gensim import *

def w2v(ds):
    sentences = LineSentence('data/transactions-%s.csv' % ds)
    trained = False
    num_iterations = 10
    if not trained:
        model = Word2Vec(sentences, size=50, window=3, min_count=5, workers=4, iter=num_iterations)
        model.save('models/basketvectors-%s.csv' % ds)
    else:
        model = Word2Vec.load('models/basketvectors-%s.csv' %ds)  # you can continue training with the loaded model!
    with open('similar_products-%s.txt' % ds, 'w') as out:
        for product in model.wv.vocab:
            similar_products = [x for x in model.most_similar(product) if x[1] > 0.4]
            if len(similar_products) > 0:
                out.write("%s is similar to %s\n" % (product, "\t".join(str(x) for x in similar_products)))

    X = model[model.wv.vocab]
    tsne = TSNE(n_components=2)
    X_tsne = tsne.fit_transform(X)


    fig, ax = plt.subplots()
    ax.scatter(X_tsne[:, 0], X_tsne[:, 1])

    for i, txt in enumerate(model.wv.vocab):
        ax.annotate(txt, (X_tsne[i, 0], X_tsne[i, 1]))

    plt.show()


w2v('repeated-2')
